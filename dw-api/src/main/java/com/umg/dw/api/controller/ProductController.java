/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umg.dw.api.controller;
import com.umg.dw.api.repository.EstudianteRepository;
import com.umg.dw.api.repository.ProductRepository;
import com.umg.dw.core.entities.Estudiante;
import com.umg.dw.core.entities.Product;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.springframework.http.ResponseEntity;

/**
 * @author vgutierrez
 */

@RestController
@RequestMapping("/producto")
//@EnableJpaRepositories(basePackages = "com.umg.dw.api.repository")


/**
 *
 * @author Victor
 */
public class ProductController {
    
    @Autowired
    ProductRepository productRepository;

    @CrossOrigin
    @GetMapping("/all")
    public ResponseEntity<List<Product>> getAllUsers(){
        return ResponseEntity.ok((List)productRepository.findAll());
    }
    
    @PostMapping("/create")
    public ResponseEntity<String> saveUser(@RequestBody Product prod){
    	
    	productRepository.save(prod);
    	
        return ResponseEntity.ok("saved");
    }
        
    
    
}
